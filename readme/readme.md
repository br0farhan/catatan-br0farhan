# Aplikasi Mobile|Kalog Prensensi|v.1.0.5+8
[![F|](https://i.postimg.cc/t4nYyNgH/powered-by-br0farhan.png)](https://www.linkedin.com/in/br0farhan/)

Aplikasi mobile presensi ini diperuntukan untuk pegawai KAI LOGISTIK untuk melakukan absensi menggunakan handphone Android / IOS.

## Fitur

- Login
- Password
- Camera
- Absensi Masuk & Absensi Pulang
- Fake Gps
- Riwayat Absensi
- Fake GPS

Aplikasi ini merupakan pengembangan dari Tim IT Development KAI LOGISTIK.
Email [kai.hansyah@gmail.com]

## Akun Saya

Jika ingin kolabarosi dan menambah pertemanan saya sangat terbuka untuk itu.

| Sosmed | Akun |
| ------ | ------ |
| Facebook | [br0Farhan][PlDb] |
| Instagram | [br0Farhan][PlGh] |
| Twitter | [br0Farhan][PlGd] |
| Linkedin | [br0Farhan][PlOd] |
| Showwcase | [br0Farhan][PlMe] |
| Website | [br0Farhan][PlGa] |



By

**Farhan Martiyansyah**

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)


   [PlDb]: <https://www.facebook.com/MyHansyah/>
   [PlGh]: <https://www.instagram.com/syahhaans/>
   [PlGd]: <https://twitter.com/br0farhan>
   [PlOd]: <https://www.linkedin.com/in/br0farhan>
   [PlMe]: <https://www.showwcase.com/br0farhan>
   [PlGa]: <https://farhanm.com/>
